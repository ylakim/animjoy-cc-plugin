Param(
  [string]$downloadUrl,
  [string]$targetFile
)

Write-Host "Animjoy-CC : Download File"
Write-Host "Source: $downloadUrl"
Write-Host "Target: $targetFile"

if( $downloadUrl -and $targetFile ) {
  $webclient = New-Object System.Net.WebClient
  $webclient.DownloadFile( $downloadUrl, $targetFile )
} else {
  Write-Host "Error: missing param(s)."
}

#Read-Host -Prompt "$line [Press Enter to exit]"