
/*
 * Original work: https://gist.github.com/mhulse/5692304983517c37cbc9
 * Improvements by @ylakim 
 */


var AI_Shell = AI_Shell || {};

AI_Shell = (function( _$this, _$application, _$window, undefined ) {

    var _private = {};

    _private.createTermFile = function( $directory, $name ) {

        var __directory = cmdFile( $directory ),
            __script    = __directory + '/' + $name + '.sh';

        var script = [
            '<?xml version="1.0" encoding="UTF-8"?>',
            '<!DOCTYPE plist PUBLIC "-//Apple Computer//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">',
            '<plist version="1.0">',
            '<dict>',
            '<key>WindowSettings</key>',
            '<array>',
            '<dict>',
            '<key>ExecutionString</key>',
            '<string>chmod u+x ' + __script + '; source ' + __script + ';</string>',
            '</dict>',
            '</array>',
            '</dict>',
            '</plist>'
        ].join( '\n' );

        return _private.createFile( $directory + '/' + $name + '.term', script );

    };

    _private.createShellScriptDemo = function( $directory, $name, $result ) {

        var script = [
            '#!/usr/bin/env bash\n',
            'cd ' + $directory,
            'curl https://dl.dropboxusercontent.com/u/1277106/data.json > ' + $result,
            'exit;'
        ].join( '\n' );

        return _private.createFile( $directory + '/' + $name + '.sh', script );

    };
    _private.createShellScript = function( command, $directory, $name, $result ) {

        var script = [
            '#!/usr/bin/env bash\n',
            //'cd ' + $directory,
            command + ' > ' + $result,
            'exit;'
        ].join( '\n' );

        return _private.createFile( $directory + '/' + $name + '.sh', script );

    };

    _private.createFile = function( $file, $string ) {

        var f = new File( $file );

        f.encoding = 'UTF-8';
        f.lineFeed = 'Unix'; // Convert to UNIX lineFeed
        // term.lineFeed = 'Windows';
        // term.lineFeed = 'Macintosh';

        f.open( 'w' );
        f.writeln( $string );
        f.close();

        return f;

    };

    _private.createDirectory = function( $directory, $remove ) {

        if( !$directory.exists ) {

            $directory.create();

        } else if( !!$remove ) {

            //_private.remove( $directory );

            //$directory.remove();

        }

    };

    _private.launchScript = function( $term ) {

        var result = $term.execute(); // Now execute the term file.

    };

    _private.readResultFile = function( /*$directory,*/ $result ) {

        var f;
        var result = '';

        f = new File( /*$directory + '/' +*/ $result );

        if( f.open( 'r' ) ) {

            result = f.read();

            f.close();

        }

        return result;

    };

    _private.parseJSON = function( $json ) {

        var data;

        eval( 'data = ' + $json );

        $.writeln( data.key ); // Should return "value".

        // Done!!!

    };

    _$this.init = function( $title ) {

        var json;
        var result;
        var directory;
        var json;
        var name;
        var term;

        name = 'tmp'; // Name to use for auto-generated files.

        result = 'result.json'; // File name of copy of remote data.

        // Determine location of "temp" folder:
        directory = new Folder( File( $.fileName ).path + '/' + name ); // Where do these scripts live?

        // Create our temporary directory:
        _private.createDirectory( directory );

        term = _private.createTermFile( directory, name );

        _private.createShellScriptDemo( directory, name, result );

        // This is just a hack to make sure the files above have been created before calling them ...
        $.sleep( 1000 );

        // ... so, this would be called after every BridgeTalk Message:
        _private.launchScript( term );

        // Another hacky way of waiting for files to get created (doing this for demo purposes):
        $.sleep( 2000 );

        // Read the newly-created data file:
        json = _private.readResultFile( directory, result );

        // Parse the JSON:
        _private.parseJSON( json );

        // Done!!!!!

    };


    /**
     * Launch shell command
     * @param {String} command
     * @param {Function} callback
     * @param {Boolean} debugCmd
     */
    _$this.launchCmd = function( command, callback, debugCmd ) {
        
        if( typeof callback !== 'function' ) {
            callback = function() {};
        }
        
        debugCmd = debugCmd || false;
        
        var _directory  = new Folder( File( $.fileName ).path ), // Location of "temp" folder
            _tempName   = 'tmp-' + currentDateForFile(), // Name to use for auto-generated files.
            _resultFile = File( $.fileName ).path + '/' + _tempName + '.txt',
            _termFile   = _private.createTermFile( _directory, _tempName );

        var _scriptFile = _private.createShellScript( command, _directory, _tempName, cmdFile( _resultFile ) );
        
        $.sleep( 1000 ); // Hack to make sure the files above have been created before calling them

        /*
         * Launch script
         */
        _private.launchScript( _termFile );
        
        var _secondsTimeout         = 30,
            _secondsAfterResponse   = 10,
            _freq                   = 500,
            _startTime              = (new Date()).getTime(),
            _cleaningFunction = function() {
                if( !debugCmd ) {
                    File( _resultFile ).remove();
                    _termFile.remove();
                    _scriptFile.remove();
                }
            },
            /*
             * Waiting function
             */
            _waitingFunction = function() {
                
                var _currentTime    = (new Date()).getTime(),
                    _hasTimedOut    = _currentTime > _startTime + _secondsTimeout * 1000;
                
                // Waiting for response
                if( !File(_resultFile).exists ) {
                    if( _hasTimedOut ) {
                        debug( 'Timeout!' );
                        _cleaningFunction();
                        return callback({
                            success: false, 
                            timeout: true, 
                            debug: {
                                startTime:      _startTime, 
                                currentTime:    _currentTime, 
                                secondsTimeout: _secondsTimeout 
                            } 
                        });
                    } else {
                        //debug( 'Waiting...' + _currentTime + ' started at ' + _startTime );
                        return setTimeout( _waitingFunction, _freq );
                    }
                    
                // Response received
                } else {
                    //debug( 'OK !' );
                    $.sleep( _secondsAfterResponse * 1000 );
                    var _resultCmd = _private.readResultFile( _resultFile );
                    _cleaningFunction();
                    return callback( _resultCmd );
                }
            };
        
        return setTimeout( _waitingFunction, _freq );
    };

    return _$this;

}( (AI_Shell || {}), app, Window ));
