Write-Host "-------------------------------------"
Write-Host "-------- Animjoy-CC : Update --------"
Write-Host "-------------------------------------"
#Write-Host "Powershell: $($PSVersionTable.PSVersion)"

$line = "`n`n"

# Config
$pluginName   = "Animjoy-CC"
$pluginFolder = "$pwd"
$parentFolder = "$pluginFolder\.."
$url          = "https://bitbucket.org/ylakim/animjoy-cc-plugin/get/HEAD.zip"
$zipName      = "animjoy-cc-plugin-latest"
$zipFolder    = "$parentFolder\$zipName"
$zipFile      = "$zipFolder.zip"
$time         = (Get-Date -format s).replace(':','-').replace('T','_')
$newFolder    = "$pluginFolder--NEW-at-$time"
$oldFolder    = "$pluginName--BACKUP-at-$time"

# Download ZIP
$webclient = New-Object System.Net.WebClient
$webclient.DownloadFile($url,$zipFile)
Write-Host "$line Telechargement OK."

# Unzip
Add-Type -AssemblyName System.IO.Compression.FileSystem
function Unzip
{
    param([string]$zipfile, [string]$outpath)
    [System.IO.Compression.ZipFile]::ExtractToDirectory($zipfile, $outpath)
}
Unzip $zipFile $zipFolder
Write-Host "$line Extraction Zip OK."

# Installation
Move-Item "$zipFolder\*" $newFolder
Write-Host "$line Installation du plugin dans:";
Write-Host " $newFolder"
Write-Host "$line Afin de finaliser l'installation, veuillez renommer $pluginName en $oldFolder"

# Cleaning
Remove-Item $zipFile
Remove-Item $zipFolder

Read-Host -Prompt "$line [Press Enter to exit]"