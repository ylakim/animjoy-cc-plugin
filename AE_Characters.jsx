﻿/*
 * Export Characters in AE
 */

//@target "aftereffects"

//@include "Helpers.jsx"
//@include "Config.jsx"

/* global app */
/* global system */
/* global Window */
/* global Folder */
/* global File */
/* global Checkbox */
/* global SaveOptions */
/* global ExportType */
/* global XMPConst */
/* global UserInteractionLevel */
/* global FolderItem */
/* global CompItem */
/* global AVLayer */
/* global TextLayer */
/* global RQItemStatus */
/* global photoshop */

var Animjoy = Animjoy || {};

Animjoy.AE_Characters = (function( _self ) {
    
    var _currentScript = (new File( $.fileName )),
        _config = {
            scriptName:     _currentScript.name.replace(/\.jsx$/, ''),
            folder:         Folder.decode( _currentScript.path + '/' ),
            outputFolder:   Folder.decode( _currentScript.path + '/output/' ),
            templates: {
                prefixLayer: '>'
            }
        },
        _templates,
        _templateComps;

    /**
     * Get template(s) for a project item (in case of a folder, recursive call will get all templates)
     * @param {Item} $projectItem
     */
    _self.getTemplatesForProjectItem = function( $projectItem ) {
        
        var _isFolder   = $projectItem instanceof FolderItem,
            _isComp     = $projectItem instanceof CompItem;

        if( _isFolder ) {
            for( var j = 1; j <= $projectItem.numItems; j++ ) {
                _self.getTemplatesForProjectItem( $projectItem.item(j) );
            }
            return;
        }
        
        if( !_isComp ) {
            return;
        }
        
        /*
         * Comp data
         */
        var $comp = $projectItem,
            _comp = {
                comp_id:    $comp.id,
                name:       $comp.name,
                project: {
                    file: File.decode( app.project.file.absoluteURI )
                },
                saved_at:   (new Date()).toString(),
                layers:     []
            },
            _layers = $comp.layers;
        
        /*
         * Layers
         */
        for( var j = 1; j <= _layers.length; j++ ) {
            var $layer = _layers[j];
            _comp.layers.push( $layer.name );
        }
        
        _templates.push( _comp );
        _templateComps.push( $comp );
    };
    
    
    /**
     * Export
     * @param {Object} options
     */
    _self.export = function( options ) {
        
        options = extend({
            export_data: true,
            export_mov: false,
            export_gif: false
        }, options || {} );
        
        clearOutput();
        
        try {
            
            // Checking version
            var _version = parseFloat( app.version );
            if( _version < 8.0 ) {
                alert( "Ce script nécessite After Effects CS3 (AE 8) ou supérieur. Vous utilisez AE " + _version + '.', _config.scriptName );
                return;
            }
            
            // Check that a project exists
            if( app.project.file === null ) {
                alert( "Veuillez créer ou ouvrir un projet et relancer le script.", _config.scriptName );
                return;
            }

            var _project = app.project;
            
            _templates = [];
            _templateComps = [];
            
            app.beginUndoGroup( _config.scriptName );
            
            /*
             * Looking for templates
             */
            for( var i = 0; i < _project.selection.length; i++ ) {
                _self.getTemplatesForProjectItem( _project.selection[i] );
            }
            
            if( _templates.length === 0 ) {
                alert( 'Sélectionnez au moins un élément du projet pour exporter des templates.');
                return;
            }
            
            /*
             * Export to file
             */
                
            if( options.export_data ) {
                createFile( _config.outputFolder + 'AE_Characters_' + currentDateForFile() + '.json', prettyJSON( _templates ) );
                writeLn( 'Export terminé à '+(new Date()).toLocaleTimeString() );
            }
            
            app.endUndoGroup();
            

        } catch( e ) {
            alert( "Une erreur est survenue : \n" + logException( e ) );
            //alert( e, _config.scriptName );
        }
          
    };
    
    
    /**
     * Visualize a character from his look code
     * @param {Object} lookLayers
     */
    _self.visualizeFromLook = function( lookLayers ) {
        
        var _compOpts = Animjoy.AE.specialComps.characters.man,
            $comp = findComp( app.project, _compOpts );
        
        if( $comp === false ) {
            alert( 'Comp not found: ' + JSON.stringify(_compOpts) );
            return;
        }
        
        _self.configureLook( $comp, lookLayers );
    };
    
    /**
     * Configure a character from his look code
     * @param {Object} lookLayers
     */
    _self.configureLook = function( $comp, lookLayers ) {
        
        // Hide all layers
        for( var j = 1; j <= $comp.layers.length; j++ ) {
            var $layer = $comp.layers[j];
            $layer.enabled = false;
        }
        
        // Show the look
        for( var key in lookLayers ) {
            var _layer = lookLayers[key];
            var $layer = findLayer( $comp, { name: _layer } );
            if( $layer !== false ) {
                $layer.enabled = true;
            }
        }
        
    };
    
    
    return _self;
    
})( Animjoy.AE_Characters || {} );