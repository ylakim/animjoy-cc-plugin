﻿/*
 * Film Production: command-line interface
 */

//@target "aftereffects"

//@include "Film_Production.jsx"

/* global Animjoy */

/* global app */
/* global RQItemStatus */
var _code, _code_str;
(function() {
    
    var _currentScript = (new File( $.fileName )),
        _config = {
            plugin:         'Animjoy',
            scriptName:     _currentScript.name.replace(/\.jsx$/, ''),
            folder:         _currentScript.path,
            templates: {
                prefixLayer: '>'
            }
        };
        
    clearOutput();
    
    var _test = testData();
    
    /**
     * Generate film from command-line
     */
    function createFilmFromCLI() {
        
        app.beginSuppressDialogs(); // careful : popups will block a script
        
        app.project.close( CloseOptions.DO_NOT_SAVE_CHANGES );
        
        app.settings.saveSetting( _config.plugin, 'projectFolder', '~/Desktop/Rendered_Films/' );
        
        /*
         * Get film code
         */
        var _filmCode = new File('~/Desktop/Rendered_Films/film_code.json');
        
        if( !_filmCode.exists ) {
            debug( File.decode( _filmCode.fsName ) + ' doesn\'t exist.' );
            return;
        }
        
        //_filmCode.copy();
        
        _filmCode.open('r');
        _filmCode.encoding = "UTF-8";
        _code_str = _filmCode.read();
        _filmCode.close();
        _code = JSON.parse( _code_str );
        
//        _code = _test;
//        _code_str = JSON.stringify( _code );
                
        /*
         * Generate film
         */
        var _success = Animjoy.Film_Production.generateFilm({
            cli:            true,
            json_str:       _code_str,
            voice_over:     true
        });
        
        /*
         * Render
         */
        if( _success && !_code.meta.render_deactivated ) {
            app.project.save();
            //app.project.close( CloseOptions.SAVE_CHANGES );
            renderFilm();
        }
    }
    
    /**
     * Render film
     * @param {Object} options
     */
    function renderFilm( options ) {
        
        debug( 'Rendering: start');
        options = extend({
        }, options || {} );
        
        var _renderQueue = app.project.renderQueue;
        
        // Clear render queue
        while( _renderQueue.numItems > 0 ) {
            _renderQueue.item( _renderQueue.numItems ).remove();
        }
            
        var $comp           = Animjoy.Film_Production.$compGlobal,
            _queueItem      = _renderQueue.items.add( $comp ), // add comp to render queue
            _outputModule   = _queueItem.outputModule( 1 ),
            _filename       = app.project.file.path + '/' + _code.meta.render_id + '.mov',
            _preset         = _code.meta.hd_video ? 'Animjoy_H264_HD' : 'Animjoy_H264_SD';
        
        _outputModule.applyTemplate( _preset );
        _outputModule.file = File( _filename );

        _queueItem.render = true;

        /* 
         * 
         */
        _queueItem.onStatusChanged = function() {
            
            var _errors = {},
                _otherStatuses = {};
        
            _errors[ RQItemStatus.WILL_CONTINUE ] = 'Rendering process has been paused (WILL_CONTINUE)';
            _errors[ RQItemStatus.NEEDS_OUTPUT ] = 'Item lacks a valid output path (NEEDS_OUTPUT)';
            _errors[ RQItemStatus.UNQUEUED ] = 'Item is listed in the Render Queue panel but composition is not ready to render (UNQUEUED)';
            _otherStatuses[ RQItemStatus.QUEUED ] = 'Composition is ready to render (QUEUED)';
            _otherStatuses[ RQItemStatus.RENDERING ] = 'Composition is rendering (RENDERING)';
            _errors[ RQItemStatus.USER_STOPPED ] = 'Rendering process was stopped by user or script (USER_STOPPED)';
            _errors[ RQItemStatus.ERR_STOPPED ] = 'Rendering process was stopped due to an error (ERR_STOPPED)';
            _otherStatuses[ RQItemStatus.DONE ] = 'Rendering process for the item is complete (DONE)';
            
            if( _errors[ _queueItem.status ] ) {
                debug( 'Error: ' + _errors[ _queueItem.status ] );
                quit();
                
            } else {
                debug( 'Status: ' + ( _otherStatuses[ _queueItem.status ] || _queueItem.status ) );
            }
            
            if( _queueItem.status === RQItemStatus.DONE ) {
                debug( 'Rendering : end (really)');
                quit();
                //system.callSystem("cmd.exe /c \"echo A message from AE >> c:\automateAE.log\"");
            }
        };
        
        // Render
        _renderQueue.render();
        
        // Handle possible app errors
        app.onError = function( err ) {
            debug( err, 'app.onError' );
            quit();
        };
    
        debug( 'Rendering : end');
        
        quit();
    }
    
    /**
     * Quits the After Effects application
     */
    function quit() {
        app.project.save();
        debug('Quitting AE...');
        app.quit();
    }
    
    
    
    function testData() {
        return {
            "meta": {
                "name": "Test"
            },
            "timeline": [
                {
                    "comp_id": 19761297,
                    "name": "Titre de la vidéo",
                    "duration": 2,
                    "layers": [
                        {
                            "index": 3,
                            "name": "Box 1",
                            "type": "image",
                            "image": {
                                "name": "BD_ustensil_Cuisine",
                                "file": "~/Creative Cloud Files/+ Icônes/BD_ustensil_Cuisine.ai",
                                "objectID": "[Général]_BD_ustensil_Cuisine",
                                "filename": "",
                                "tpl": {
                                    "start_2_columns": false,
                                    "end_2_columns": false
                                }
                            }
                        },
                        {
                            "index": 2,
                            "name": "Titre",
                            "type": "text",
                            "text": "Hello World with é ô"
                        }
                    ],
                    "filename": "comp19761297",
                    "is_layout": false,
                    "objectID": "comp19761297",
                    "text": "Hello with é ô",
                    "annotation": ""
                }
            ]
        };
    }
    
    
    /*
     * Launch CLI script
     */
    try {
        createFilmFromCLI();

    } catch( e ) {
        writeLn('Une erreur est survenue.');
        alert( e, _config.scriptName );
    }
    
    app.quit();

})();