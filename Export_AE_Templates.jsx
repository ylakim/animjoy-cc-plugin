﻿/*
 * Export AE Templates
 */

//@target "aftereffects"

//@include "Helpers.jsx"
//@include "Config.jsx"

/* global app */
/* global system */
/* global Window */
/* global Folder */
/* global File */
/* global Checkbox */
/* global SaveOptions */
/* global ExportType */
/* global XMPConst */
/* global UserInteractionLevel */
/* global FolderItem */
/* global CompItem */
/* global AVLayer */
/* global TextLayer */
/* global RQItemStatus */
/* global photoshop */

var Animjoy = Animjoy || {};

Animjoy.Export_AE_Templates = (function( _self ) {
    
    var _currentScript = (new File( $.fileName )),
        _config = {
            scriptName:     _currentScript.name.replace(/\.jsx$/, ''),
            folder:         Folder.decode( _currentScript.path + '/' ),
            outputFolder:   Folder.decode( _currentScript.path + '/output/' ),
            templates: {
                prefixLayer: '>'
            }
        },
        _templates,
        _templateComps;

    /**
     * Get template(s) for a project item (in case of a folder, recursive call will get all templates)
     * @param {Item} $projectItem
     */
    _self.getTemplatesForProjectItem = function( $projectItem ) {
        
        var _isFolder   = $projectItem instanceof FolderItem,
            _isComp     = $projectItem instanceof CompItem;

        if( _isFolder ) {
            for( var j = 1; j <= $projectItem.numItems; j++ ) {
                _self.getTemplatesForProjectItem( $projectItem.item(j) );
            }
            return;
        }
        
        if( !_isComp ) {
            return;
        }
        
        var _exportAll = _self.options.export_all;
        
        /*
         * Comp data
         */
        var $comp = $projectItem,
            _comp = {
                comp_id:    $comp.id,
                name:       $comp.name,
                project: {
                    file: File.decode( app.project.file.absoluteURI ),
                    org: {
                        folder:     $comp.parentFolder.name,
                        folders:    getItemFolders( $comp )
                    }
                },
                comment:    $comp.comment,
                width:      $comp.width,
                height:     $comp.height,
                duration:   $comp.duration,
                saved_at:   (new Date()).toString(),
                layers:     []
            },
            _layers = $comp.layers,
            _prefix = _config.templates.prefixLayer;
        
        _comp.filename = _comp.comp_id.toString();
        _comp.objectID = _comp.filename;
        
        _comp.is_layout = ( _comp.project.org.folders.join('/').search('Layout') !== -1 );
        
        var _tag            = _comp.project.org.folders.join().toLowerCase().search('ooreka') !== -1 ? 'Ooreka' : 'Général',
            _tagFolder      = toPermalink( _tag ) + '/',
            _gifFilename    = _comp.filename + '.mov.gif';
        _comp.gif_s3_key  = Animjoy.S3.folders.animations + _tagFolder + _gifFilename;
        _comp.gif_url     = Animjoy.S3.bucket + _comp.gif_s3_key;
        
        /*
         * Layers
         */
        for( var j = 1; j <= _layers.length; j++ ) {
            var $layer      = _layers[j],
                _isVariable = ( $layer.name.search( _prefix ) === 0 ),
                _keepLayer  = _exportAll || _isVariable,
                _cleanName  = $layer.name.slice( _isVariable ? _prefix.length : 0 ).trim();
        
            if( $layer.canSetCollapseTransformation && !$layer.collapseTransformation ) {
                //debug( 'collapseTransformation = false for layer "' + $layer.name + '" in comp "' + $comp.name + '"' );
                $layer.locked = false;
                $layer.collapseTransformation = true;
            }

            if( !_keepLayer ) {
                continue;
            }
            
            var _layer = {
                    index:  $layer.index,
                    name:   _cleanName,
                    type:   ''
                };

            // Image / solid / comp / audio...
            if( $layer instanceof AVLayer ) {

                var _sourceAV   = $layer.source, // AVItem: FootageItem or CompItem
                    _isFootage  = _sourceAV instanceof FootageItem,
                    _isSolid    = _isFootage && _sourceAV.mainSource instanceof SolidSource,
                    _isImage    = _isFootage && _sourceAV.mainSource instanceof FileSource,
                    _isBox      = isBoxLayer( $layer );

                /* 
                 * Image
                 */
                if( _isImage ) {

                    _layer.type = 'image';

                    var _sourceFS       = $layer.source.file.toString(),
                        _filename_ai    = File.decode( $layer.source.name ),
                        _name           = _filename_ai.replace(/\.ai/, ''),
                        _pngFilename    = toPermalink( _name ) + '.png',
                        _tag            = _sourceFS.toLowerCase().search('ooreka') !== -1 ? 'Ooreka' : 'Général',
                        _tagFolder      = toPermalink( _tag ) + '/';

                    _layer.image = {
                        file:           File.decode( _sourceFS ),
                        name:           _name,
                        png_url:        Animjoy.S3.bucket + Animjoy.S3.folders.images + _tagFolder + _pngFilename
                    };

                /*
                 * Box (layout element)
                 */
                } else if( _isBox ) {

                    _layer.type = 'box';
                    _layer.box = {

                        width:          $layer.width,
                        height:         $layer.height,

                        anchorPoint:    $layer.anchorPoint.value,
                        position:       $layer.position.value,
                        scale:          $layer.scale.value,
                        rotation:       $layer.rotation.value
                    };

                } else {
                    if( !_exportAll ) {
                        continue;
                    }
                }

            /*
             * Texte 
             */
            } else if( $layer instanceof TextLayer ) {
                var _textDoc = $layer.sourceText.value;
                _layer.text  = _textDoc.text;
                _layer.type   = 'text';
                if( _exportAll ) {
                    _layer.style = {
                        fontFamily:     _textDoc.font,
                        fontSize:       _textDoc.fontSize,
                        fontStyle:      _textDoc.fontStyle,
                        color:          arrayRgbToHex( _textDoc.fillColor ),
                        textAlign:      textAlignFromTextJustification( _textDoc.justification )
                    };
                }
            } else {
                if( !_exportAll ) {
                    continue;
                }
            }

            if( _exportAll ) {
                _layer.box = {
                    width:          $layer.width,
                    height:         $layer.height,

                    anchorPoint:    $layer.anchorPoint.value,
                    position:       $layer.position.value,
                    scale:          $layer.scale.value,
                    rotation:       $layer.rotation.value,
                    opacity:        $layer.opacity.value
                };
            }

            _comp.layers.push( _layer );
        }
        
        _comp.layers.sort( sortNamesAlphabeticallySpecial );


        _templates.push( _comp );
        _templateComps.push( $comp );
    };
    
    /**
     * Render template comps
     * @param {Object} options
     */
    _self.renderCompositions = function( options ) {
        
        options = extend({
            export_gif: false,
            export_gif_with_DropToGIF: false,
            export_gif_with_Photoshop: false
        }, options || {} );
        
        if( !_templateComps.length ) {
            return;
        }
        
        var _renderQueue = app.project.renderQueue;
        
        // Clear render queue
        while( _renderQueue.numItems > 0 ) {
            _renderQueue.item( _renderQueue.numItems ).remove();
        }
        
        for( var i = 0; i < _templateComps.length; i++ ) {
            
            var _comp           = _templateComps[i],
                _queueItem      = _renderQueue.items.add( _comp ), // add comp to render queue
                _outputModule   = _queueItem.outputModule( 1 ),
                _filename       = _config.outputFolder + 'mov/' + _templates[i].filename + '.mov';
            
            if( typeof _queueItem.setSettings === 'function' ) { // since AE 13 (CC 2014): https://blogs.adobe.com/creativecloud/new-changed-after-effects-cc-2014
                _queueItem.setSettings({
                    'Resolution' : 'Quarter'
                });
            }
            
            //debug( _outputModule.name );
            _outputModule.file = File( _filename );
            
            _queueItem.render = true;
            
            /* 
             * GIF conversion
             */
            if( options.export_gif ) {
                _queueItem.onStatusChanged = function() {
                    if( _queueItem.status === RQItemStatus.DONE ) {
                        
//                        // Drop to GIF (for each video)
//                        if( options.export_gif_with_DropToGIF ) {
//                            //var _gifApp = _config.folder + 'lib/Gifrocket.app',
//                            var _gifApp = _config.folder + 'lib/Drop to GIF.app',
//                                _cmd    = 'open -g -n -a "' + _gifApp + '" "' + _filename + '"',
//                                _res    = system.callSystem( _cmd );
//                            debug( _cmd );
//                            $.sleep( 3000 );                            
//                        }
                        
                        if( options.export_gif_with_Photoshop ) {
                            photoshop.open( new File( _outputModule.file ) );
                        }
                    }
                };
            }
        }
        
        // GIF conversion: open "Drop to GIF" (/output/mov/ must be set as a watch folder)
        if( options.export_gif && options.export_gif_with_DropToGIF ) {
            var _gifApp = _config.folder + 'lib/Drop to GIF.app',
                _cmd    = 'open -a "' + _gifApp + '"';
            system.callSystem( _cmd );
            $.sleep( 3000 );
        }
        
        // Render
        _renderQueue.render();
        
        // Handle possible app errors
        app.onError = function( err ) {
            debug( err, 'app.onError' );
        };
    };
    
    /**
     * Default options
     * @returns {Literal}
     */
    _self.exportDefaults = function() {
        return {
            export_data: true,
            export_all: false,
            export_mov: false,
            export_gif: false
        };
    };
    
    /**
     * Init
     * @param {Object} options
     */
    _self.init = function( options ) {
        
        _self.options = extend( _self.exportDefaults(), options || {} );
        
        clearOutput();
        
        try {
            
            // Checking version
            var _version = parseFloat( app.version );
            if( _version < 8.0 ) {
                alert( "Ce script nécessite After Effects CS3 (AE 8) ou supérieur. Vous utilisez AE " + _version + '.', _config.scriptName );
                return;
            }
            
            // Check that a project exists
            if( app.project.file === null ) {
                alert( "Veuillez créer ou ouvrir un projet et relancer le script.", _config.scriptName );
                return;
            }

            var _project = app.project;
            
            _templates = [];
            _templateComps = [];
            
            app.beginUndoGroup( _config.scriptName );
            
            /*
             * Looking for templates
             */
            for( var i = 0; i < _project.selection.length; i++ ) {
                _self.getTemplatesForProjectItem( _project.selection[i] );
            }
            
            if( _templates.length === 0 ) {
                alert( 'Sélectionnez au moins un élément du projet pour exporter des templates.');
                return;
            }
            
            /*
             * Export to file
             */
            
            var _export = {
                    meta: {
                        project: File.decode( _project.file.absoluteURI ),
                        date: (new Date).toString(),
                        time: (new Date).getTime()/1000,
                        system: {
                            osName: system.osName,
                            osVersion: system.osVersion,
                            userName: system.userName,
                            machineName: system.machineName
                        }
                    },
                    templates: _templates
                };
                
            if( _self.options.export_data ) {
                //createFile( _config.outputFolder + 'AE_Templates_' + currentDateForFile()+ '.json', prettyJSON( _export ) );
                
                var _fsExport = _config.outputFolder + 'AE_Templates_Algolia_' + currentDateForFile() + '.json';
                createFile( _fsExport, prettyJSON( _templates ) );
                
                var $export = File( _fsExport );
                if( $export.exists ) {
                    $export.execute();
                }
                writeLn( 'Export terminé à '+(new Date()).toLocaleTimeString() );
            }
            
            app.endUndoGroup();
            
            /*
             * Render compositions (for animated GIFs)
             */
            //app.beginUndoGroup( _config.scriptName );
            
            if( _self.options.export_mov ) {
                _self.renderCompositions( _self.options );
            }
            
            //app.endUndoGroup();

        } catch( e ) {
            alert( "Une erreur est survenue : \n" + logException( e ) );
            //alert( e, _config.scriptName );
        }
          
    };
    
    return _self;
    
})( Animjoy.Export_AE_Templates || {} );