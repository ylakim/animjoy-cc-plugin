
var Animjoy = Animjoy || {};

/*
 * Amazon S3
 */
Animjoy.S3 = {
    bucket: 'https://animjoy.s3-eu-west-1.amazonaws.com/',
    folders: {
        animations: 'assets/animations/',
        images:     'assets/images/'
    }
};

/*
 * App
 */
Animjoy.App = {
    prod_url:   'https://www.animjoy.com',
    local_url:  'http://animjoy.local',
    use_prod: true
};

/*
 * Creative Cloud Files
 */
Animjoy.CC = {
    folders: {
        root:       '~/Creative Cloud Files/Animjoy/',
        assets:     '~/Creative Cloud Files/Animjoy/Assets/',
        images:     '~/Creative Cloud Files/Animjoy/Assets/Images/',
        effects:    '~/Creative Cloud Files/Animjoy/Assets/Animations/Effects/',
    }
};

/*
 * After Effects
 */
Animjoy.AE = {
    project: '~/Creative Cloud Files/Animjoy/Assets/Animations/Template_Animation_Animjoy_V01.aep',
    specialComps: {
        subtitles: { name: 'Subtitles' },
        neutral: { name: 'Neutral Animation' },
        entities: {
            LOCATION: { name: 'Geo_World_Map_Framed' },
            ORGANIZATION: { name: 'Organization' },
            EVENT_NORMAL: { name: 'Event (Normal)' },
            EVENT_HEADER: { name: 'Event (Header)' },
            NUMBER: { name: 'Number' },
            ACRONYM: { name: 'Acronym' },
            DATE_DAY: { name: 'Date (Day + Month + Year)' },
            DATE_MONTH: { name: 'Date (Month + Year)' },
            IMAGE_STACK_2_ELEMENTS: { name: 'Image Stack (2 elements)' },
            IMAGE_STACK_3_ELEMENTS: { name: 'Image Stack (3 elements)' }
        },
        characters: {
            frame_full_body: { name: 'Frame Character (Full Body)' },
            masculine: { name: 'Character_Combination_Man' },
            feminine: { name: 'Character_Combination_Woman' }
        }
    },
    specialLayers: {
        locationMap: { name: 'Map' },
        markerName: function( iMarker ) { return 'Marker #' + iMarker; }
    },
    effects: {
        scale: 'Scale.ffx'
    },
    bonusTimeAtCompEnd: 1
};

/*
 * Plugin
 */
Animjoy.Plugin = {
    UI: {
        logo: 'images/logo_animjoy_small.png'
    },
    logsFolder: (new File( $.fileName )).path + '/logs/'
};