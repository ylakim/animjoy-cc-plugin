﻿/*
 * Animjoy UI
 */

//@target "aftereffects"

/* global Animjoy */
/* global Mustache */

/* global app */
/* global system */
/* global Window */
/* global Panel */
/* global Folder */
/* global File */
/* global Checkbox */

(function( thisObj ) {
    
    /*
     * Checking plugin folder
     */
    var _currentScript      = (new File( $.fileName )),
        _scriptUIFolderName = 'ScriptUI Panels',
        _pluginFolderName   = 'Animjoy-CC-Plugin',
        _inPluginFolder     = Folder.decode( _currentScript.parent.name ) === _pluginFolderName,
        _inScriptUIFolder   = Folder.decode( _currentScript.parent.name ) === _scriptUIFolderName,
        _pluginFolder       = _inPluginFolder ? _currentScript.parent : Folder( _currentScript.parent.path + '/' + _pluginFolderName ),
        _scriptUIFolder     = _inScriptUIFolder ? _currentScript.parent : Folder( _currentScript.path + '/../' + _scriptUIFolderName ),
        _scriptsFolder      = _pluginFolder.parent;
    
    if( !_pluginFolder.exists ) {
        alert( 'Le plugin "Animjoy-CC" est introuvable dans le dossier "After Effects/Scripts".' );
        return;
    }
    if( _inPluginFolder ) {
        alert( 'Le fichier "Animjoy.jsx" doit être copié dans le dossier "After Effects/Scripts/ScriptUI Panels".' );
        return;
    }
    
    /*
     * Includes
     */
    //@include "../Animjoy-CC-Plugin/Helpers.jsx"
    //@include "../Animjoy-CC-Plugin/Export_AE_Templates.jsx"
    //@include "../Animjoy-CC-Plugin/AE_Characters.jsx"
    //@include "../Animjoy-CC-Plugin/Film_Production.jsx"
    
    var _config = {
            plugin:         'Animjoy',
            scriptName:     _currentScript.name.replace(/\.jsx$/, ''),
            folder:         Folder.decode( _pluginFolder.fsName + '/' ),
            outputFolder:   Folder.decode( _pluginFolder.fsName + '/output/' ),
            logsFolder:     Folder.decode( _pluginFolder.fsName + '/logs/' ),
            installed : true
        },
        _settings = {
            projectFolder: app.settings.haveSetting( _config.plugin, 'projectFolder' ) ? app.settings.getSetting( _config.plugin, 'projectFolder' ) : '~/Desktop'
        };
    
    /**
     * Copy this script to the "ScriptUI Panels" folder (if necessary)
     * @returns {bool} states if the script should continue
     */
    function install() {
        
        if( _config.installed || _inScriptUIFolder ) {
            return true;
        }
        
        if( !_scriptUIFolder.exists ) {
            _scriptUIFolder.create();
        }
        
        var _targetScript = _scriptUIFolder.fsName + '/' + _config.plugin + '.jsx',
            _copied = _currentScript.copy( _targetScript );
        
        if( _copied ) {
            var _targetFile = new File( _targetScript );
            alert( "Veuillez redémarrer After Effects pour terminer l'installation de " + _config.plugin, _config.scriptName );
            //$.evalFile( _targetFile );
        }
        
        return false;
    }
    
    /**
     * This function builds the user interface.
     * @param {Panel} thisObj Panel object (if script is launched from Window menu); null otherwise.
     * @returns {Window|Panel} Window or Panel object representing the built user interface.
     */
    function buildUI( thisObj ) {
        
        var _positionPal = (app.settings.haveSetting( _config.plugin, "frameBounds" ) && !(thisObj instanceof Panel));
        
        if( _positionPal ) {
            var _bounds = [];
            _bounds = app.settings.getSetting( _config.plugin, "frameBounds" ).split(',');
            for( var i in _bounds ) {
                _bounds[i] = parseInt( _bounds[i], 10 );
            }
        } else {
            var _bounds = undefined;
        }

        var $palette = (thisObj instanceof Panel) ? thisObj : new Window( "palette", _config.scriptName, _bounds, { resizeable: true } );

        if( $palette === null ) {
            return $palette;
        }
        
        function updateUILayout( $container ) {
            $container.layout.layout( true ); //Update the container  
            $palette.layout.layout( true ); //Then update the main UI layout  
        }
        
        $palette.margins = 5;
        $palette.spacing = 5;
        $palette.alignment = 'fill';
        //$palette.maximumSize.width = 100;
        
        $palette.logo = $palette.add( "image", undefined, File( _config.folder + Animjoy.Plugin.UI.logo ) );
        //$palette.logo.alignment = 'left';
        
        var $tabs = $palette.add( "tabbedpanel", undefined );
        $tabs.margins = 0;
        $tabs.spacing = 0;
        $tabs.alignment = 'fill';
        
        /* ----------------
         *    Import tab
         * --------------- */
        
        var $importTab = $tabs.add( "tab", undefined, 'Import' );
        $importTab.margins = 15;
        $importTab.spacing = 10;
        
        $importTab.add( "statictext", undefined, 'Copiez-collez le code du film :' );
        
        var $filmContent = $importTab.add( "edittext", undefined, '',  { multiline: true } );
        $filmContent.alignment = 'fill';
        $filmContent.preferredSize.height = 70;
        //$palette.graphics.font = "dialog:32";
        //$palette.graphics.backgroundColor = $palette.graphics.newBrush( $palette.graphics.BrushType.SOLID_COLOR, [ 0.5, 0.0, 0.0, 1 ] );
        $filmContent.active = true;
        
        var $voiceOver = $importTab.add( "checkbox", undefined, 'Créer une voix-off automatique' );
        $voiceOver.value = true;
        
        var $btnCreateFilm = $importTab.add( "button", undefined, 'Générer le film' );
        $btnCreateFilm.onClick = function() {
            $filmContent.enabled = $btnCreateFilm.enabled = false;
            var _btnOriginalText = $btnCreateFilm.text;
            $btnCreateFilm.text = 'Génération...';
            var _success = Animjoy.Film_Production.generateFilm({
                json_str:       $filmContent.text,
                voice_over:     $voiceOver.value
            });
            $filmContent.text = '';
            $btnCreateFilm.text = _btnOriginalText;
            $filmContent.enabled = $btnCreateFilm.enabled = true;
        };
        
        var $logMsg = $importTab.add( "statictext", undefined, '', { multiline: true } );
        //$logMsg.maximumSize.width = 200;
        
        
        /* ------------------
         *    Settings tab
         * ------------------ */
        
        var $settingsTab = $tabs.add( "tab", undefined, 'Paramètres' );
        $settingsTab.margins = 15;
        $settingsTab.spacing = 15;
        //$settingsTab.alignChildren = 'left';
        
        /* 
         * Project folder
         */
        var _msgProjectFolder = 'Dossier où sauvegarder les nouveaux projets AE',
            $panelProjectFolder = $settingsTab.add( "panel", undefined, _msgProjectFolder );
        $panelProjectFolder.alignment = 'fill';
        
        var $projectFolder = $panelProjectFolder.add( "statictext", undefined, _settings.projectFolder, { multiline: true } );
        $projectFolder.alignment = 'fill';
        
        var $btnProjectFolder = $panelProjectFolder.add( "button", undefined, 'Modifier' );
        $btnProjectFolder.onClick = function() {
            var _selectedFolder     = Folder.selectDialog( _msgProjectFolder ),
                _selectedFolderPath = Folder.decode( _selectedFolder.fsName );
            if( _selectedFolder !== null ) {
                _settings.projectFolder = _selectedFolderPath;
                $projectFolder.text     = _selectedFolderPath;
                app.settings.saveSetting( _config.plugin, 'projectFolder', _selectedFolderPath );
            }
        };
        
        /*
         * Self-update script
         */
        var $panelUpdatePlugin = $settingsTab.add( "panel", undefined, 'Mise à jour' );
        $panelUpdatePlugin.alignment = 'fill';
        
        var $topUpdatePlugin = $panelUpdatePlugin.add( "statictext", undefined, 'Télécharger la dernière version de BenoîtBot.', { multiline: true } );
        $topUpdatePlugin.alignment = 'fill';
        
        var $btnUpdatePlugin = $panelUpdatePlugin.add( "button", undefined, 'Mettre à jour' );
        
        //var $updatePluginText = $panelUpdatePlugin.add( "statictext", undefined, "\n" + "\n" + "\n" + "\n" + "\n", { multiline: true } );
        var $updatePluginText = $panelUpdatePlugin.add( "listbox", undefined, [] );        
        $updatePluginText.alignment = 'fill';
        $updatePluginText.preferredSize.height = 60;
        
        var _updatePlugin = {
                logLines: []
            };
        _updatePlugin.showLog = function() {
            //$updatePluginText.text = _updatePlugin.logLines.join("\n");
            //$updatePluginText.preferredSize = [-1,-1];
            debug( _updatePlugin.logLines.join(' // ') );
            if( $updatePluginText.items.length > 0 ) {
                $updatePluginText.selection = $updatePluginText.items[ $updatePluginText.items.length - 1 ];
            }
            updateUILayout( $panelUpdatePlugin );
            //$palette.layout.layout(true);
            //$palette.layout.resize();
        };
        _updatePlugin.addLog = function( _txt ) {
            _updatePlugin.logLines.push( _txt );
            $updatePluginText.add( "item", _txt );
            _updatePlugin.showLog();
        };
        _updatePlugin.replaceLog = function( _txt ) {
            _updatePlugin.logLines.pop();
            _updatePlugin.logLines.push( _txt );
            $updatePluginText.remove( $updatePluginText.items[ $updatePluginText.items.length - 1 ] );
            $updatePluginText.add( "item", _txt );
            _updatePlugin.showLog();
        };
        
        $btnUpdatePlugin.onClick = function() {
            
            try {
                
                $btnUpdatePlugin.enabled = false;

                /*
                 * Clean previous downloads
                 */
                _updatePlugin.addLog( 'Préparation...' );
                
                var _existingDownloads = _scriptsFolder.getFiles( 'ylakim-animjoy-cc-plugin-*' );
                for( var i = 0; i < _existingDownloads.length; i++ ) {
                    _existingDownloads[i].remove();
                }
                
                var _zipFilename = _scriptsFolder.fsName + '/animjoy-cc-plugin-latest.zip',
                    _zipFile = File( _zipFilename );
                if( _zipFile.exists ) {
                    _zipFile.remove();
                }
                
                _updatePlugin.replaceLog('Préparation : OK');
                
                /*
                 * Download from BitBucket repo
                 */
                _updatePlugin.addLog( 'Téléchargement : en cours...' );
                
                var _zipUrl     = 'https://bitbucket.org/ylakim/animjoy-cc-plugin/get/HEAD.zip',
                    _cmd        = 'curl "' + _zipUrl + '" > "' + _zipFilename + '"',
                    _resCurl    = system.callSystem( _cmd ),
                    _downloaded = File( _zipFilename ).exists;
                if( !_downloaded ) {
                    throw "Téléchargement impossible.";
                }
                
                _updatePlugin.replaceLog( 'Téléchargement : ' + ( _downloaded ? 'OK' : 'Erreur' ) );

                /*
                 * Unzip
                 */
                var _cmdUnzip       = 'unzip "' + _zipFilename + '" -d "' + Folder.decode( File( _zipFilename ).path ) + '"',
                    _resUnzip       = system.callSystem( _cmdUnzip ),
                    _unzippedFolder = _scriptsFolder.getFiles( 'ylakim-animjoy-cc-plugin-*' ),
                    _unzipped       = _unzippedFolder.length > 0;
                _updatePlugin.addLog( 'Décompression ZIP : ' + ( _unzipped ? 'OK' : 'Erreur' ) );
                if( !_unzipped ) {
                    throw "Erreur concernant la décompression ZIP.";
                }
                
                /*
                 * Rename old and new plugin folders
                 */
                if( isNotAdmin() ) {
                    
                    var _newPluginFolder = _unzippedFolder[0];

                    _pluginFolder.rename( _pluginFolder.name + '__backup_' + currentDateForFile() );

                    _newPluginFolder.rename( _pluginFolderName );

                    var _scriptUIFile   = File( _newPluginFolder.fsName + '/' + _currentScript.name ),
                        _copied         = _scriptUIFile.copy( _scriptUIFolder.fsName + '/' + _scriptUIFile.name );

                    _updatePlugin.addLog( 'Installation : ' + ( _copied ? 'OK' : 'Erreur' ) );

                    if( _copied ) {
                        _updatePlugin.addLog( 'Redémarrez BenoîtBot via le menu "Fenêtre" !' );
                    }
                    
                } else {
                    _updatePlugin.addLog( "Installation : partielle (car admin)" );
                }
                
                /*
                 * Cleaning
                 */
                File( _zipFilename ).remove();
                
            } catch( e ) {
                logFile( 'self-update_errors', e );
            }
            
            $btnUpdatePlugin.enabled = true;
        };
        
        
        /* ------------------------
         *    Admin / export tab
         * ------------------------ */
        
        if( isAdmin() ) {
            
            var $exportTab = $tabs.add( "tab", undefined, 'Admin' );
            $tabs.selection = $exportTab;
            $exportTab.margins = 15;
            $exportTab.spacing = 15;
            $exportTab.alignment = 'fill';
            //$exportTab.add( "scrollbar", undefined );

            /*
             * Export animations
             */
            var $exportAnimations = $exportTab.add( "panel", undefined, 'Animations After Effects' );
            $exportAnimations.margins = 15;
            $exportAnimations.alignment = 'fill';
            $exportAnimations.alignChildren = "left";

            $exportAnimations.export_data  = $exportAnimations.add( "checkbox", undefined, 'Exporter les données JSON' );
            $exportAnimations.export_data.enabled  = false;
            $exportAnimations.export_all   = $exportAnimations.add( "checkbox", undefined, 'Exporter tous les calques' );
            $exportAnimations.export_mov   = $exportAnimations.add( "checkbox", undefined, 'Exporter les vidéos MOV' );
            $exportAnimations.export_gif   = $exportAnimations.add( "checkbox", undefined, 'Exporter les images GIF' );
            $exportAnimations.export_gif.onClick = function() {
                if( $exportAnimations.export_gif.value === true ) {
                    $exportAnimations.export_mov.value = true;
                }
            };
            
            var _exportDefaults = Animjoy.Export_AE_Templates.exportDefaults();
            for( var _option in _exportDefaults ) {
                if( $exportAnimations[ _option ] instanceof Checkbox ) {
                    $exportAnimations[ _option ].value = _exportDefaults[_option];
                }
            }

            var $btnExportTemplates = $exportAnimations.add( "button", undefined, 'Exporter les templates' );
            $btnExportTemplates.alignment = 'center';
            $btnExportTemplates.onClick = function() {
                try {
                    $btnExportTemplates.enabled = false;
                    var _options = {};
                    
                    for( var _option in _exportDefaults ) {
                        if( $exportAnimations[ _option ] instanceof Checkbox ) {
                            _options[ _option ] = $exportAnimations[ _option ].value;
                        }
                    }
                        
                    Animjoy.Export_AE_Templates.init( _options );
                    $btnExportTemplates.enabled = true;
                } catch( e ) {
                    alert( "Une erreur est survenue : \n" + logException( e ) );
                }
            };

            /*
             * Export characters
             */
            var $exportCharacters = $exportTab.add( "panel", undefined, 'Personnages' );
            $exportCharacters.margins = 15;
            $exportCharacters.alignment = 'fill';
            $exportCharacters.alignChildren = "left";
            
            $exportCharacters.export_data  = $exportCharacters.add( "checkbox", undefined, 'Exporter les données JSON' );
            $exportCharacters.export_data.value    = true;
            $exportCharacters.export_data.enabled  = false;
            
            var $btnExportCharacters = $exportCharacters.add( "button", undefined, 'Exporter les personnages' );
            $btnExportCharacters.alignment = 'center';
            $btnExportCharacters.onClick = function() {
                try {
                    $btnExportCharacters.enabled = false;
                    var _options = {
                            export_data:    $exportCharacters.export_data.value
                        };
                    Animjoy.AE_Characters.export( _options );
                    $btnExportCharacters.enabled = true;
                } catch( e ) {
                    alert( "Une erreur est survenue : \n" + logException( e ) );
                }
            };
            
            /*
             * Visualize a character
             */
            
            $exportTab.add( "statictext", undefined, 'Visualiser un personnage :' );

            var $characterLook = $exportTab.add( "edittext", undefined, '',  { multiline: true } );
            $characterLook.alignment = 'fill';
            $characterLook.preferredSize.height = 20;
            $characterLook.active = true;

            var $btnShowCharacter = $exportTab.add( "button", undefined, 'Visualiser' );
            $btnShowCharacter.onClick = function() {
                try {
                    $btnShowCharacter.enabled = false;
                    Animjoy.AE_Characters.visualizeFromLook( JSON.parse( $characterLook.text ) );
                    $btnShowCharacter.enabled = true;
                } catch( e ) {
                    alert( "Une erreur est survenue : \n" + logException( e ) );
                }
            };
            
            

//            var $exportImages = $exportTab.add( "panel", undefined, 'Images Illustrator' );
//            $exportImages.margins = 15;
//            $exportImages.alignment = 'fill';
//
//            var $btnExportImages = $exportImages.add( "button", undefined, 'Exporter les images' );
//            $btnExportImages.onClick = function() {
//                var _filename   = _pluginFolder.fsName + '/Illustrator.jsx',
//                    _scriptFile = new File( _filename );
//                if( _scriptFile.exists ) {
//                    //_scriptFile.execute();
//                    illustrator.executeScript( _scriptFile.read() );                
//                    
//                } else {
//                    alert( "Script introuvable: " + _scriptFile, _config.scriptName );
//                }
//            };
            
        }

        
        /*
         * Finishing UI
         */
        $palette.layout.layout(true);
        $palette.layout.resize();
        $palette.onResizing = $palette.onResize = function() {
            this.layout.resize();
        };

        return $palette;
    }

    /**
     * Launch sandbox
     */
    function sandbox() {
        launchScript( 'Sandbox.jsx' );
    }

    // This callback function runs the selected script.
    function launchScript( filename ) {
        
        var _filename   = _currentScript.path + '/' + filename,
            _scriptFile = new File( _filename );
        
        if( _scriptFile.exists ) {
            $.evalFile( _scriptFile );
        } else {
            alert( "Script introuvable: " + _scriptFile, _config.scriptName );
        }

    }
    
    /**
     * Init
     */
    function init() {
        
        clearOutput();
        
        try {
            
            // Checking version
            var _version = parseFloat( app.version );
            if( _version < 10.0 ) {
                alert( "Ce script nécessite After Effects CS5 (AE 10) ou supérieur. Vous utilisez AE " + _version + '.', _config.scriptName );
                return;
            }
            
            /*var _installed = install();
            if( !_installed ) {
                return;
            }*/
            
            // Build and show the palette
            var $paletteUI = buildUI( thisObj );
            if( $paletteUI === null ) {
                return;
            }

            if( $paletteUI instanceof Window ) {
                $paletteUI.show();
            } else {
                $paletteUI.layout.layout( true );
            }
            
            
            
        } catch( e ) {
            alert( "Une erreur est survenue : \n" + logException( e ) );
        }
    }
    
    init();
    
})( this );
