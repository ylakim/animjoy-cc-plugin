﻿/*
 * Film Production
 */

//@target "aftereffects"

//@include "Helpers.jsx"
//@include "Config.jsx"
//@include "AE_Characters.jsx"
//@include "lib/extendscript.geo.jsx"

/* global Mustache */

/* global app */
/* global system */
/* global Folder */
/* global File */
/* global Checkbox */
/* global CompItem */
/* global AVLayer */
/* global TextLayer */
/* global FootageItem */
/* global SolidSource */
/* global FileSource */
/* global RQItemStatus */
/* global CloseOptions */
/* global KeyframeInterpolationType */
/* global photoshop */

var Animjoy = Animjoy || {};

Animjoy.Film_Production = (function( _self ) {
    
    var _currentScript = (new File( $.fileName )),
        _config = {
            plugin:         'Animjoy',
            scriptName:     _currentScript.name.replace(/\.jsx$/, ''),
            folder:         Folder.decode( _currentScript.path + '/' ),
            outputFolder:   Folder.decode( _currentScript.path + '/output/' ),
            templates: {
                prefixLayer: '>'
            }
        };
    
    /**
     * Generate film (AE project and compositions) from JSON timeline
     * @param {Object} options
     * @returns {Boolean} success
     */
    _self.generateFilm = function( options ) {
        
        var _options = extend({
            cli: false,
            json_str: '',
            json: {
                meta: {
                    name:       '',
                    lang:       '',
                    render_id:  '',
                    add_effects:  false
                },
                timeline: []
            },
            voice_over: false,
            projectFolder: app.settings.haveSetting( _config.plugin, 'projectFolder' ) ? app.settings.getSetting( _config.plugin, 'projectFolder' ) : '~/Desktop'
        }, options );
        
        try {
            
            if( app.project.file !== null ) {
                alert("Vous devez d'abord fermer le projet actuellement ouvert.");
                return false;
            }
            
            //app.beginUndoGroup( _config.scriptName );
            
            /*
             * Import templates project
             */
            var _json_str           = ( _options.json_str || '' ).trim(),
                _json               = extend( _options.json, JSON.parse( _json_str ) ),
                _templateFile       = Animjoy.AE.project,
                _templateProject    = File( _templateFile ),
                _projectName        = ( _json.meta.render_id || _json.meta.name || 'Film' ).toString().removeDiacritics(),
                _date               = currentDateForFile(),
                _renderName         = _json.meta.render_id ? _json.meta.render_id : _projectName + ' (' + _date + ')',
                _projectFolder      = Folder( _options.projectFolder + '/' + _renderName ),
                _projectFolderFS    = Folder.decode( _projectFolder.fsName );
            
            if( !_projectFolder.create() ) {
                var _msg = "Le dossier du film n'a pu être créé (" + _projectFolderFS + ').';
                alert( _msg  );
                debug( _msg );
                return false;
            }
            
            Animjoy.Plugin.logsFolder = _projectFolder.fsName + '/';
            
            var _newProject         = File( _projectFolderFS + '/AE_project.aep' );
            
            app.open( _templateProject );
            var _saved = app.project.save( _newProject );
            if( !_saved ) {
                var _msg = "L'import ne peut continuer car le projet n'a pas été sauvegardé.";
                alert( _msg );
                debug( _msg );
                app.project.close( CloseOptions.DO_NOT_SAVE_CHANGES );
                return false;
            }

            var _timelineFolder = app.project.items.addFolder("Timeline"),
                _precompFolder  = app.project.items.addFolder("Precomps"),
                _imagesFolder   = app.project.items.addFolder("Images"),
                _audioFolder    = app.project.items.addFolder("Audio"),
                _compSubsOpts   = Animjoy.AE.specialComps.subtitles,
                $compSubs       = findComp( app.project, _compSubsOpts ),
                $layerSub       = $compSubs instanceof CompItem ? $compSubs.layer(1) : false,
                $compAnnotation = findComp( app.project, Animjoy.AE.specialComps.neutral ),
                $layerAnnotation = $compAnnotation instanceof CompItem ? $compAnnotation.layer(1) : false,
                _tempFiles      = [];
            
            if( $layerSub === false ) {
                var _errorMsg = Mustache.render( 'La composition de sous-titres est introuvable ({{{info}}})', { info: _compSubsOpts.name } );
                writeLn( _errorMsg );
                debug( _errorMsg );
            }
            
            var $compGlobal = app.project.items.addComp( 'Timeline / Montage', 1920, 1080, 1, 10, 25 );
            $compGlobal.bgColor = [ 1, 1, 1 ];
            $compGlobal.openInViewer();
            _self.$compGlobal = $compGlobal;
            var _currentTime = 0;
            
            /*
             * Create compositions
             */
            for( var i = 0; i < _json.timeline.length; i++ ) {
                
                var _comp       = _json.timeline[i],
                    $tplComp    = findComp( app.project, { id: _comp.comp_id } );

                if( !( $tplComp instanceof CompItem ) ) {
                    continue;
                }
                
                var $comp = $tplComp.duplicate();
                $comp.parentFolder = _timelineFolder;
                $comp.name = '[' + (i+1) + '] ' + $tplComp.name;
                
                /**
                 * Update layers from a comp
                 * @param {CompItem} $comp
                 * @param {Object} compData
                 * @param {CompItem} $parentComp
                 * @param {integer} depth
                 */
                function updateCompLayers( $comp, compData, $parentComp, depth ) {
                    
                    for( var j = 0; j < compData.layers.length; j++ ) {
                        
                        var _layer      = compData.layers[j],
                            $layer      = findLayer( $comp, { placeholder: _layer.name } );
                    
                        if( $layer === false ) {
                            debug( 'Layer not found (placeholder = ' + _layer.name + ') in "' + $comp.name + '" comp.' );
                            continue;
                        }
                    
                        var _sourceAV   = $layer.source, // AVItem: FootageItem or CompItem
                            _isText     = $layer instanceof TextLayer,
                            _isFootage  = _sourceAV instanceof FootageItem,
                            _isImage    = _isFootage && _sourceAV.mainSource instanceof FileSource,
                            _isBox      = isBoxLayer( $layer );

                        $layer.locked = false;
                        
                        /*
                         * Text
                         */
                        if( _isText ) {
                            $layer.property( "sourceText" ).setValue( _layer.text );
                        }

                        /*
                         * Image
                         */
                        if( _layer.type === 'image' ) {
                            
                            var $newImage,
                                _vectorFs       = _layer.image.file,
                                _vectorFile     = File( _vectorFs ),
                                _EPS_file       = File( _vectorFile.getEPSfilename() ),
                                _importFile     = _isBox && _EPS_file.exists ? _EPS_file : _vectorFile;
                            
                            if( _importFile.isImportableVector() ) {
                                if( _importFile.exists ) {
                                    $newImage = app.project.importFile( new ImportOptions( _importFile ) );
                                    
                                    if( _isImage ) {
                                        $layer.replaceSource( $newImage, true );
                                    } else if( _isBox ) {
                                        replaceLayerInBox( $layer, $newImage );
                                    }
                                    
                                    $newImage.parentFolder = _imagesFolder;
                                    
//                                    if( _json.meta.add_effects ) {
//                                        applyAnimationPreset( $layer, Animjoy.AE.effects.scale );
//                                    }
                                    
                                } else {
                                    debug( 'Missing image: ' + _importFile.fsName );
                                }
                                
                            } else {
                                debug( 'Image not importable: ' + _importFile.fsName );
                            }
                            
                        }

                        /*
                         * Precomp
                         */
                        if( _layer.type === 'precomp' ) {

                            var $tplPreComp = findComp( app.project, { id: _layer.precomp.comp_id } );
                            if( !( $tplPreComp instanceof CompItem ) ) {
                                continue;
                            }

                            var $preComp = $tplPreComp.duplicate();
                            $preComp.parentFolder = _precompFolder;
                            
                            depth++;
                            $preComp.name = '[' + (i+1) + '.' + depth + '] ' + _layer.name;

                            if( _isBox ) {
                                replaceLayerInBox( $layer, $preComp );
                            } else {
                                $layer.replaceSource( $preComp, true );
                            }
                            
                            $layer.collapseTransformation = false;
                            
                            updateCompLayers( $preComp, _layer.precomp, $comp, depth );
                        }

                        /*
                         * Special layer: location, organization, event...
                         */
                        if( _layer.type === 'special' && _layer.special_data ) {

                            var _specialData    = _layer.special_data,
                                _entityType     = _specialData.special_type || _layer.nlp.type,
                                _isPersonaLook  = _entityType === 'PERSONA_LOOK',
                                _isLocation     = _entityType === 'LOCATION',
                                _isDate         = _entityType === 'DATE',
                                _specialComp;
                            
                            switch( _entityType ) {
                                
                                case 'PERSONA_LOOK':
                                    _specialComp = Animjoy.AE.specialComps.characters.frame_full_body;
                                    break;
                                
                                case 'EVENT':
                                    var _isHeader = _layer.name === 'Header';
                                    _specialComp = Animjoy.AE.specialComps.entities[ _isHeader ? 'EVENT_HEADER' : 'EVENT_NORMAL' ];
                                    break;
                                    
                                case 'DATE':
                                    var _isDay = _specialData.day !== null;
                                    if( _isDay ) {
                                        _specialComp = Animjoy.AE.specialComps.entities.DATE_DAY;
                                    } else {
                                        _specialComp = Animjoy.AE.specialComps.entities.DATE_MONTH;
                                    }
                                    break;
                                    
                                case 'IMAGE_STACK':
                                    var _countStackedImages = _specialData.images.length;
                                    if( _countStackedImages === 2 ) {
                                        _specialComp = Animjoy.AE.specialComps.entities.IMAGE_STACK_2_ELEMENTS;
                                    } else {
                                        _specialComp = Animjoy.AE.specialComps.entities.IMAGE_STACK_3_ELEMENTS;
                                    }
                                    break;
                                    
                                default:
                                    _specialComp = Animjoy.AE.specialComps.entities[ _entityType ];
                                    break;
                            }
                            
                            var $tplPreComp = findComp( app.project, _specialComp );
                            if( !( $tplPreComp instanceof CompItem ) ) {
                                debug( 'No comp found for ' + _entityType + ' entity: ' + JSON.stringify( _specialComp ) );
                                continue;
                            }

                            var $preComp = $tplPreComp.duplicate();
                            $preComp.parentFolder = _precompFolder;
                            
                            //depth++;
                            $preComp.name = '[' + (i+1) + '.' + depth + '] ' + _layer.name;

                            if( _isBox ) {
                                replaceLayerInBox( $layer, $preComp, { no_padding: _isLocation } );
                            } else {
                                $layer.replaceSource( $preComp, true );
                            }
                            
                            $layer.collapseTransformation = _isDate || _isPersonaLook;
                            
                            switch( _entityType ) {
                                
                                /*
                                 * Location
                                 */
                                case 'LOCATION':
                                    
                                    var $layerMap = findLayer( $preComp, Animjoy.AE.specialLayers.locationMap );
                                    var _markers = _specialData.markers;
                                    
                                    if( !_markers || $layerMap === false || !( $layerMap.source instanceof CompItem ) ) {
                                        break;
                                    }
                                    
                                    var $map = $layerMap.source.duplicate();
                                    $layerMap.replaceSource( $map, true );
                                    
                                    var $center     = findLayer( $map, { name: 'Center' } ),
                                        $markerTpl  = findLayer( $map, { name: 'Marker Template' } ),
                                        $zone       = findLayer( $map, { name: 'Zone' } );
                                    
                                    if( $center === false || $markerTpl === false || $zone === false ) {
                                        break;
                                    }
                                    
                                    // Hide elements that should be hidden :)
                                    $markerTpl.enabled = false;
                                    $center.enabled = false;
                                    $zone.enabled = false;

                                    var _boxMarkers = {
                                        xMin: $map.width,
                                        xMax: 0,
                                        yMin: $map.height,
                                        yMax: 0
                                    };
                                    
                                    /*
                                     * Markers position & containing box
                                     */
                                    for( var iMarker = 0; iMarker < _markers.length; iMarker++ ) {
                                        
                                        var _marker     = _markers[iMarker],
                                            _geo        = _marker.geo || {},
                                            _latitude   = _geo.lat,
                                            _longitude  = _geo.lon;
                                        
                                        if( typeof _latitude === 'undefined' || typeof _longitude === 'undefined' ) {
                                            continue;
                                        }
                                        
                                        var $marker = $markerTpl.duplicate();

                                        var _pixels     = Geo.projections.ae.mercator.toAESpace( { lat: _latitude, lng: _longitude }, 1, $map.width, $map.height ),
                                            _position   = $marker.position.value;
                                        _position[0] = _pixels.x;
                                        _position[1] = _pixels.y;
                                        $marker.position.setValue( _position );
                                        $marker.enabled = true;
                                        $marker.name = Animjoy.AE.specialLayers.markerName( iMarker );
                                        
                                        if( _pixels.x < _boxMarkers.xMin ) {
                                            _boxMarkers.xMin = _pixels.x;
                                        }
                                        if( _pixels.y < _boxMarkers.yMin ) {
                                            _boxMarkers.yMin = _pixels.y;
                                        }
                                        if( _pixels.x > _boxMarkers.xMax ) {
                                            _boxMarkers.xMax = _pixels.x;
                                        }
                                        if( _pixels.y > _boxMarkers.yMax ) {
                                            _boxMarkers.yMax = _pixels.y;
                                        }
                                    }
                                    
                                    /*
                                     * Zone calculation
                                     */
                                    var _padding = ( _markers.length > 1 ) ? 100 : 300,
                                        _boxWithPadding = {
                                            xMin: _boxMarkers.xMin - _padding,
                                            xMax: _boxMarkers.xMax + 0.5*_padding,
                                            yMin: _boxMarkers.yMin - 0.5*_padding,
                                            yMax: _boxMarkers.yMax + 0.5*_padding
                                        };
                                    
                                    _boxWithPadding.width   = _boxWithPadding.xMax - _boxWithPadding.xMin;
                                    _boxWithPadding.height  = _boxWithPadding.yMax - _boxWithPadding.yMin;
                                    
                                    var _ratios = {
                                            box:    _boxWithPadding.width / _boxWithPadding.height,
                                            zone:   $zone.width / $zone.height
                                        };
                                    
                                    var _scaleZone = 100 * ( _ratios.box > _ratios.zone ? _boxWithPadding.width / $zone.width : _boxWithPadding.height / $zone.height );
                                    
                                    $zone.scale.setValue([ _scaleZone, _scaleZone, _scaleZone ]);
                                    
                                    var _center = {
                                        x: ( _boxWithPadding.xMax + _boxWithPadding.xMin ) / 2,
                                        y: ( _boxWithPadding.yMax + _boxWithPadding.yMin ) / 2
                                    };
                                    
                                    $center.position.setValue([ _center.x, _center.y, $center.position.value[2] ]);
                                    
                                    break;
                                
                                /*
                                 * Organization
                                 */
                                case 'ORGANIZATION':
                                    var $layerName = findLayer( $preComp, { name: 'Name' } );
                                    if( $layerName !== false ) {
                                        $layerName.sourceText.setValue( _specialData.org_name || '' );
                                    }
                                    break;
                                
                                /*
                                 * Event
                                 */
                                case 'EVENT':
                                    var $layerName = findLayer( $preComp, { name: 'Name' } );
                                    if( $layerName !== false ) {
                                        $layerName.sourceText.setValue( _specialData.event_name || '' );
                                    }
                                    break;
                                
                                /*
                                 * Number
                                 */
                                case 'NUMBER':
                                    var $layerName = findLayer( $preComp, { name: 'Name' } );
                                    if( $layerName !== false ) {
                                        $layerName.sourceText.setValue( _specialData.content || _layer.nlp.content || '' );
                                    }
                                    break;
                                
                                /*
                                 * Acronym
                                 */
                                case 'ACRONYM':
                                    var $layerName = findLayer( $preComp, { name: 'Name' } );
                                    if( $layerName !== false ) {
                                        $layerName.sourceText.setValue( _specialData.content || _layer.nlp.content || '' );
                                    }
                                    break;
                                
                                /*
                                 * DATE
                                 */
                                case 'DATE':
                                    var $layerDate;
                                    $layerDate = findLayer( $preComp, { name: 'Day' } );
                                    if( $layerDate !== false ) {
                                        $layerDate.sourceText.setValue( _specialData.day || '' );
                                    }
                                    $layerDate = findLayer( $preComp, { name: 'Month' } );
                                    if( $layerDate !== false ) {
                                        $layerDate.sourceText.setValue( _specialData.month_name || '' );
                                    }
                                    $layerDate = findLayer( $preComp, { name: 'Year' } );
                                    if( $layerDate !== false ) {
                                        $layerDate.sourceText.setValue( _specialData.year || '' );
                                    }
                                    break;
                                    
                                /*
                                 * Image stack
                                 */
                                case 'IMAGE_STACK':
                                    for( var indexIS = 0; indexIS < _specialData.images.length; indexIS++ ) {
                                        
                                        var $layerIS = findLayer( $preComp, { name: '> Box ' + ( indexIS + 1 ) } );
                                        if( $layerIS === false ) {
                                            continue;
                                        }
                                        
                                        var _stackedImage = _specialData.images[indexIS],
                                            _isBox = true,
                                            $newImage,
                                            _vectorFs       = _stackedImage.file,
                                            _vectorFile     = File( _vectorFs ),
                                            _EPS_file       = File( _vectorFile.getEPSfilename() ),
                                            _importFile     = _isBox && _EPS_file.exists ? _EPS_file : _vectorFile;

                                        if( _importFile.isImportableVector() ) {
                                            if( _importFile.exists ) {
                                                $newImage = app.project.importFile( new ImportOptions( _importFile ) );
                                                replaceLayerInBox( $layerIS, $newImage );
                                                $newImage.parentFolder = _imagesFolder;

                                            } else {
                                                debug( 'Missing image: ' + _importFile.fsName );
                                            }

                                        } else {
                                            debug( 'Image not importable: ' + _importFile.fsName );
                                        }
                                    }                                    
                                    break;
                                    
                                /*
                                 * Persona look
                                 */
                                case 'PERSONA_LOOK':
                                    var _personaLook = _specialData.persona_look || {},
                                        _gender = _personaLook.gender,
                                        _charComp = Animjoy.AE.specialComps.characters[ _gender ],
                                        $tplCharacter = findComp( app.project, _charComp ),
                                        $layerChar = $preComp.layer(1);
                                    
                                    if( $tplCharacter !== false ) {
                                        var $character = $tplCharacter.duplicate();
                                        $layerChar.replaceSource( $character, true );
                                        Animjoy.AE_Characters.configureLook( $character, _personaLook );
                                        
                                    } else {
                                        $layerChar.remove();
                                        debug('Removing character layer: no comp found (' + JSON.stringify(_charComp) + ')' );
                                    }                                    
                                    
                                    break;
                            }
                        }
                        
                    }
                    
                    /*
                     * Audio + subtitles + annotation
                     */
                    createVoiceOverAndTexts( $comp, compData, $parentComp, depth );
                    
                    
                    /*
                     * Word-based timing
                     */
                    if( depth === 0 ) {
                        for( var j = 0; j < compData.layers.length; j++ ) {
                            
                            var _layer      = compData.layers[j],
                                $layer      = findLayer( $comp, { placeholder: _layer.name } );
                            if( $layer === false ) {
                                continue;
                            }
                            if( typeof _layer.nlp !== 'undefined' && 
                                typeof _layer.nlp.beginOffsetInSentence !== 'undefined' && 
                                typeof _layer.nlp.sentenceLength !== 'undefined' ) {
                                var _percent    = _layer.nlp.beginOffsetInSentence / _layer.nlp.sentenceLength,
                                    _startTime  = _percent * $comp.duration;
                                $layer.startTime = _startTime;
                                $layer.inPoint   = _startTime;
                                $layer.outPoint  = $comp.duration + Animjoy.AE.bonusTimeAtCompEnd;
                            }
                            
                            /*
                             * Timing in location map
                             */
                            if( _layer.type === 'special' && _layer.nlp.type === 'LOCATION' ) {
                                
                                var $preComp = $layer.source;
                                if( !( $preComp instanceof CompItem ) ) {
                                    continue;
                                }
                                
                                var $layerMap = findLayer( $preComp, Animjoy.AE.specialLayers.locationMap );
                                if( $layerMap === false ) {
                                    continue;
                                }
                                
                                $map = $layerMap.source;
                                
                                var _markers = _layer.special_data.markers;
                                
                                for( var iMarker = 0; iMarker < _markers.length; iMarker++ ) {
                                    var _marker     = _markers[iMarker],
                                        $marker     = findLayer( $map, { name: Animjoy.AE.specialLayers.markerName( iMarker ) } );
                                    if( $marker === false ) {
                                        continue;
                                    }
                                    if( typeof _marker.nlp !== 'undefined' && 
                                        typeof _marker.nlp.beginOffsetInSentence !== 'undefined' && 
                                        typeof _marker.nlp.sentenceLength !== 'undefined' ) {
                                        var _percent    = ( _marker.nlp.beginOffsetInSentence - _markers[0].nlp.beginOffsetInSentence ) / _marker.nlp.sentenceLength,
                                            _startTime  = _percent * $comp.duration;
                                        $marker.startTime = _startTime;
                                        $marker.inPoint   = _startTime;
                                        $marker.outPoint  = $comp.duration;
                                    }
                                }
                            }
                        }
                    }
                    
                    /*
                     * End of the comp (bonus time, time remapping)
                     */
                    if( compData.is_layout ) {
                        
                        // Add bonus time at the end
                        $comp.duration          = $comp.duration + Animjoy.AE.bonusTimeAtCompEnd;
                        $comp.workAreaDuration  = $comp.duration;
                        
                        // Hold footage (precomps) until the end with time remapping
                        for( var j = 1; j <= $comp.layers.length; j++ ) {
                            var $layer = $comp.layers[j],
                                _isBox = isBoxLayer( $layer, true );
                            if( _isBox ) {
                                $layer.locked = false;

                                // Hold last image (time remapping, if needed)
                                if( $layer.canSetTimeRemapEnabled && $layer.outPoint !== $comp.duration ) {
                                    $layer.timeRemapEnabled = true;

                                    var _timeLastFrame  = $layer.outPoint - $comp.frameDuration,
                                        _timeRemap      = $layer.property('Time Remap');

                                    _timeRemap.addKey( _timeLastFrame );
                                    _timeRemap.removeKey( _timeRemap.numKeys );

                                    _timeRemap.setInterpolationTypeAtKey( _timeRemap.numKeys, KeyframeInterpolationType.LINEAR, KeyframeInterpolationType.HOLD );

                                    $layer.outPoint = $comp.duration;
                                }
                            }
                        }
                    }
                    
                    /*
                     * Cleaning
                     */
                    cleanCompLayers( $comp );
                    
                }
                
                /**
                 * Clean a comp
                 * @param {CompItem} $comp
                 */
                function cleanCompLayers( $comp ) {                    
                    for( var j = 1; j <= $comp.layers.length; j++ ) {
                        var $layer              = $comp.layers[j],
                            _hasSpecialName     = $layer.name.search('--') === 0,
                            _isEmptyBox         = isBoxLayer( $layer, false ),
                            _clean              = _hasSpecialName || _isEmptyBox;
                        if( _clean ) {
                            $layer.locked = false;
                            $layer.remove();
                            j--;
                        }
                    }
                }
                
                updateCompLayers( $comp, _comp, null, 0 );
                
                /**
                 * Create voice-over, subtitles and annotation
                 * @param {CompItem} $comp
                 * @param {Object} compData
                 * @param {integer} depth
                 */
                function createVoiceOverAndTexts( $comp, compData, $parentComp, depth ) {
                    
                    var _text           = (compData.text || '').trim(),
                        _audioUrl       = compData['audio_url'] || '',
                        _subtitles      = _text.split("\n"),
                        _finalSubs      = [],
                        _subDuration    = 1.5;
                    
                    /*
                     * Process subtitles (cut sentences too long)
                     */
                    for( var j = 0; j < _subtitles.length; j++ ) {
                        var _subtitle   = _subtitles[j].trim(),
                            _maxChars   = 110;
                        
                        if( _subtitle.length < _maxChars ) {
                            _finalSubs.push( _subtitle );
                        } else {
                            var _cutSubs = splitStringWithoutCuttingWords( _subtitle, _maxChars );
                            _finalSubs = _finalSubs.concat( _cutSubs );
                        }
                    }
                    _subtitles = _finalSubs;
                    
                    /* 
                     * Text-to-speech
                     */
                    var $voiceLayer = null;
                    if( _options.voice_over && _text && _audioUrl ) {
                        
                        var _isWindows      = $.os.indexOf("Windows") !== -1,
                            _suffix         = '_' + (i+1) + '.' + depth,
                            _audioExtension = _isWindows ? 'mp3' : 'aiff',
                            _soundFS        = _projectFolderFS + '/VoiceOver' + _suffix + '.' + _audioExtension,
                            _soundFile      = File( _soundFS );
                        
                        /*
                         * Download/create audio file
                         */
                        
                        // Windows
                        if( _isWindows ) {
                            
                            _audioUrl = _audioUrl.replace( 'https', 'http' );
                            downloadBinaryFile( _audioUrl, _soundFile );
                            
                            /*_cmd = Mustache.render( 'Powershell.exe -ExecutionPolicy Unrestricted -File "{{{ps_script}}}" "{{{audio_url}}}" "{{{audio_file}}}"', {
                                ps_script:     File( _config.folder + 'Download_File.ps1' ).fsName,
                                audio_url:      _audioUrl,
                                audio_file:     _soundFS 
                            });*/
                            
                        // Mac
                        } else {
                            
                            // Create text file
                            var _textFS         = _projectFolderFS + '/Texte' + _suffix + '.txt',
                                _textFile       = createFile( _textFS, _subtitles.join(' ') );
                            
                            var _lang           = _json.meta.lang || 'fr',
                                _voices = {
                                    'fr' : 'Thomas', // default French voice on Mac OS X
                                    'en' : 'Samantha',
                                },
                                _voice          = _voices[ _lang ] || _voices['fr'];
                            
                            // Create text-to-speech
                            var _cmd = Mustache.render(
                                    'say -v {{{voice}}} -r {{{speech_rate}}} -o {{{sound_file}}} -f {{{text_file}}}', {
                                    voice:          _voice,
                                    speech_rate:    170, // words per minute
                                    sound_file:     cmdFile( _soundFS ),
                                    text_file:      cmdFile( _textFS )
                                }),
                                _resCmd         = system.callSystem( _cmd );
                            if( !_resCmd ) {
//                                debug( _cmd );
//                                debug( _resCmd );
                                _tempFiles.push( _textFile );
                            } else {
                                debug( _cmd );
                                debug( _resCmd );
                            }
                        }
                        
                        // Import audio file
                        if( _soundFile.exists ) {
                            var _voiceFile = app.project.importFile( new ImportOptions( _soundFile ) );
                            _voiceFile.parentFolder = _audioFolder;
                            _subDuration = _voiceFile.duration / _subtitles.length;
                            $voiceLayer = $comp.layers.add( _voiceFile );
                        }
                    
                    }

                    /*
                     * Subtitles
                     */
                    if( $layerSub !== false ) {
                        for( var j = 0; j < _subtitles.length; j++ ) {

                            var _subtitle = _subtitles[j].trim();

                            if( !_subtitle ) {
                                continue;
                            }

                            var $subtitle = copyLayerToComp( $layerSub, $comp );

                            $subtitle.sourceText.setValue( _subtitle );
                            $subtitle.name = '';

                            $subtitle.startTime = 0;
                            $subtitle.inPoint   = ( j ) * _subDuration;
                            $subtitle.outPoint  = ( j + 1 ) * _subDuration;
                            
                            if( $parentComp instanceof CompItem ) {
                                var $subtitleParentComp = copyLayerToComp( $subtitle, $parentComp ),
                                    $compLayerInParent  = findLayer( $parentComp, { comp_id: $comp.id });
                                if( $compLayerInParent !== false ) {
                                    $subtitleParentComp.startTime = $compLayerInParent.inPoint;
                                } else {
                                    debug( 'Layer not found (comp #' + $comp.id + ' / "' + $comp.name + '") in "' + $parentComp.name + '" comp (for subtitle)' );
                                }
                                $subtitle.enabled = false;
                            }
                        }
                    }

                    if( $voiceLayer instanceof AVLayer ) {
                        $voiceLayer.moveToBeginning();
                        if( $voiceLayer.outPoint > $comp.duration ) {
                            $comp.duration          = $voiceLayer.outPoint;
                            $comp.workAreaStart     = 0;
                            $comp.workAreaDuration  = $comp.duration;
                        }
                        if( $voiceLayer.hasAudio ) {
                            $voiceLayer.audioLevels.setValue( [ 10, 10 ] ); // 10 dB
                        }
                    }

                    /*
                     * Add annotation for animator
                     */
                    if( $layerAnnotation !== false && compData.annotation ) {
                        var $newLayerAnnotation = copyLayerToComp( $layerAnnotation, $comp );
                        $newLayerAnnotation.sourceText.setValue( compData.annotation );
                        $newLayerAnnotation.startTime = 0;
                        $newLayerAnnotation.inPoint   = 0;
                        $newLayerAnnotation.outPoint  = $comp.duration;
                    }
                }
                

                /*
                 * Add comp to global timeline
                 */
                var $layerCompInGlobal = $compGlobal.layers.add( $comp, $comp.duration );
                $layerCompInGlobal.name = i+1;
                $layerCompInGlobal.startTime = _currentTime;
                _currentTime += $comp.duration;
            
            } // End of timeline loop
            
            // Adjust global timeline
            if( $layerCompInGlobal instanceof AVLayer && $layerCompInGlobal.outPoint > $compGlobal.duration ) {
                $compGlobal.duration = $layerCompInGlobal.outPoint;
            }
            
            /*
             * Cleaning
             */
            app.project.reduceProject( [ _timelineFolder, $compGlobal ] );
            app.project.save();
            for( var i = 0; i < _tempFiles.length; i++ ) {
                var _tempFile = _tempFiles[i];
                if( _tempFile instanceof File ) {
                    _tempFile.remove();
                }
            }
            
            //app.endUndoGroup();
            return true;
            
        } catch( e ) {
            logFile( 'generate_film', logException( e ) );
            //writeLn( 'Erreur: ' + e );
            alert( "Une erreur est survenue : \n" + logException( e ) );
            //app.endUndoGroup();
            return false;
        }
        
    };

    
    return _self;
    
})( Animjoy.Film_Production || {} );