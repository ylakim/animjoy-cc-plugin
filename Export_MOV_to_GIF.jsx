﻿/*
 * Convert MOV videos to animated GIF with Photoshop
 */

//@target "photoshop"

/* global Animjoy */

/* global app */
/* global Folder */
/* global File */
/* global ExportType */
/* global SaveOptions */
/* global SaveDocumentType */
/* global Dither */
/* global MatteType */
/* global Palette */
/* global DialogModes */

(function() {
    
    app.displayDialogs = DialogModes.NO; // Suppress warning dialogs

    // Source files
    var _sourceFiles = File.openDialog( 'Sélectionnez les fichiers MOV à convertir en GIF', '*.mov', true );
    if( !_sourceFiles || !_sourceFiles.length ) {
        alert( 'Aucun fichier trouvé.' );
        return;
    }
    
    // Destination to save the files
    var _defaultFolder = _sourceFiles[0].parent,
        //_exportFolder = Folder( _defaultFolder.fsName + '/- PNG' );
        _exportFolder = Folder.selectDialog( 'Sélectionnez le dossier dans lequel vous souhaitez enregistrer les fichiers GIF.', _defaultFolder );
    
    
    var _numFiles = _sourceFiles.length;
    
    try {
        for( var i = 0; i < _numFiles; i++ ) {

            var _sourceFile = _sourceFiles[i],
                _sourceDoc  = app.open( _sourceFile ), //app.activeDocument,
                _file       = _sourceDoc.name.split('.'),
                _filename   = _file[0];
            
            // Export options
            var _opts = new ExportOptionsSaveForWeb();
            _opts.format = SaveDocumentType.COMPUSERVEGIF;
            _opts.colors = 128;
            _opts.dither = Dither.PATTERN;
            _opts.ditherAmount = 100;
            _opts.interlaced = false;
            _opts.matte = MatteType.NONE;
            _opts.palette = Palette.LOCALSELECTIVE;
            _opts.transparency = true;

            // Export
            var _gifFile = File( _exportFolder + '/' + _filename + '.gif' );
            _sourceDoc.exportDocument( _gifFile, ExportType.SAVEFORWEB, _opts );
            
            _sourceDoc.close( SaveOptions.DONOTSAVECHANGES );
        
        }
        
    } catch( e ) {
        //alert( "Une erreur est survenue : \n" + logException( e ) );
        alert( "Une erreur est survenue : \n" + e );
    }
    
})();
