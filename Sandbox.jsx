﻿/*
 * Sandbox
 */

//@target "aftereffects"

//@include "Helpers.jsx"

////@include "lib/GetURLs.jsx"
//@include "lib/json2.js"
//@include "lib/http-client/lib/http-client.full.jsx"
//@include "lib/adobe-javascript-http-client/http.jsx"
//@include "lib/extendscript.geo.jsx"

/* global app */

(function() {
    
    var _currentScript = (new File( $.fileName )),
        _config = {
            scriptName:     _currentScript.name.replace(/\.jsx$/, ''),
            folder:         _currentScript.path,
            templates: {
                prefixLayer: '>'
            }
        };
        
    clearOutput();
    app.beginUndoGroup( _config.scriptName );
        
    function testHTTP() {

        var _url;
        _url = 'umaps.fr';
        _url = 'adobe.com';

        var _response = $http({
            method: 'GET',
            //url: 'http://date.jsontest.com/'
            //url: 'http://benoitdessine.com/',
            url: 'http://animjoy.s3-eu-west-1.amazonaws.com/films/audio/60a89d7e_0_1491326031.mp3'
        });

        debug( _response.payload );
        createBinaryFile( 'C:/Users/Administrator/Desktop/createBin2.mp3', _response.payload );

//        var _client = new HttpClient( _url, {
//            encoding: 'utf8',
//            timeout: 10,
//            headers: {
//                "User-Agent": "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)"
//            }
//        });
//
//        _client.get( '/', function(response) {
//            writeLn( "StatusCode:    " + response.statusCode );
//            createFile( _config.folder + '/exports/http.log', response.body );
//        });

    }
    
    function testCompAndLayers() {
        
        try {
            var $comp = app.project.activeItem,
                $layer = $comp.selectedLayers[0];
            
            
            $layer.timeRemapEnabled = false;
            $layer.timeRemapEnabled = true;
            
            var _timeLastFrame = $layer.outPoint - $comp.frameDuration,
                _timeRemap = $layer.property('Time Remap');
            
            _timeRemap.addKey( _timeLastFrame );
            _timeRemap.removeKey( _timeRemap.numKeys );
            
            _timeRemap.setInterpolationTypeAtKey( _timeRemap.numKeys, KeyframeInterpolationType.LINEAR, KeyframeInterpolationType.HOLD );
            
            $layer.outPoint  = $comp.duration;
            
        } catch( e ) {
            alert( logException( e ) );
        }

        
    }
    
    function testCreateAudio() {
        
        var _projectName        = 'Sant\u00E9',
            _projectFolderFS    = '/Users/Mikaly/Desktop/Tests AE/' + _projectName,
            _date               = currentDateForFile(),
            _subtitles          = [ 'Vous souffrez de crampes nocturnes aux mollets ?' ],
            i = 1;
        //debug( File.encode( "Santé" ) );
        var _suffix         = '_' + ( i+1 ),
            _textFS         = _projectFolderFS + '/Texte' + _suffix + '.txt',
            _textFile       = createFile( _textFS, _subtitles.join(' ') ),
            _soundFS        = _projectFolderFS + '/Voix-off_auto' + _suffix + '.aiff',
            _voice          = 'Thomas', // default French voice on Mac OS X
            _cmd = Mustache.render(
                'cd {{{directory}}}; say -v {{{voice}}} -r {{{speech_rate}}} -o "{{{sound_file}}}" -f "{{{text_file}}}"', {
                voice:          _voice,
                speech_rate:    200,
                directory:      cmdFile( _textFile.parent.fsName ),
                sound_file:     'test.aiff',
                text_file:      cmdFile2( _textFile.name )
            }),
            _resCmd         = system.callSystem( _cmd );
        debug( _cmd );
        debug( _resCmd );
    }
    
    function helloWorld() {
        //alert( 'Hello World! \n' + (new Date()).toString() );
        
        var _filmCode = File('~/Desktop/Rendered_Films/test.json');
     
        _filmCode.open('r');
        _filmCode.encoding = "UTF-8";
        var _code_str = _filmCode.read();
        _filmCode.close();
        
        //alert( _code_str );
        var _ref = "~/Creative Cloud Files/+ Icônes/BD_Homme_Nicolas.ai";
        
        alert( _code_str + "\n" + _ref + "\n => " + ( _code_str === _ref ? 'equals' : 'NOT equals' ) );
        
        return;
    }
    
    function downloadFile() {
//        var _cmd = 'Powershell.exe -ExecutionPolicy Unrestricted -File "C:\Users\Administrator\Documents\Adobe Scripts\Animjoy-CC\Download_File.ps1" "https://animjoy.s3-eu-west-1.amazonaws.com/films/audio/60a89d7e_0_1491326031.mp3" "C:\Users\Administrator\Desktop\yo.mp3"';
//        debug( _cmd );
////            _resCmd = system.callSystem( _cmd );
//        //var timeStr = system.callSystem('cmd.exe /c "time /t"');
//        var _cmd = 'cmd.exe /c "' + _cmd.replace('"','\"') + '"';
//        var _resCmd = system.callSystem( _cmd );
//        //alert("Result: " + timeStr);
//        debug( _cmd );  
//        debug( _resCmd );

        var _url = "http://animjoy.s3-eu-west-1.amazonaws.com/films/audio/60a89d7e_0_1491326031.mp3",
            _imageFile = File( "C:\\Users\\Administrator\\Desktop\\Geturl4.mp3" );
        downloadBinaryFile( _url, _imageFile );
    }
    
    function geoCalc() {
        
        var _latitude = 42.697283,
            _longitude = 9.450880999999981,
            _result = Geo.projections.ae.mercator.toAESpace( { lat: _latitude, lng: _longitude }, 1, 2000, 2000 );
        
        alert( _result.x + ' / ' + _result.y );
    }
    
    function applyEffect() {
        try {

            var $comp = app.project.activeItem,
            $layer = $comp.layer(1),
            $layer2 = $comp.layer(2);
            
            for( var iEffect = 1; iEffect <= $layer.effect.numProperties; iEffect++ ) {
                var $effect = $layer.effect(iEffect);
                copyPropertyBase( $effect, $layer2 ); // cf http://aenhancers.com/viewtopic.php?t=2464
            }
            
            
            //$layer2.effects.setValue = $layer.effects;
            //alert('done');
            
        } catch( e ) {
            alert( logException( e ) );
        }

    
//        var _presetsFolder = Folder( Folder( Folder.appPackage.absoluteURI ).parent.absoluteURI + "/Presets" );
//
//        var _presetFile = new File( _presetsFolder.fsName + "/Transitions - Movement/Zoom - spirale.ffx" );
//
//        if( _presetFile.exists ) {
//            $layer.applyPreset( _presetFile );
//        } else {
//            alert('No preset found at ' + _presetFile.fsName );
//        }

    }
    
    
    try {
        
        //testHTTP();        
        //testCompAndLayers();
        //testCreateAudio();
        //helloWorld();
        //downloadFile();
        //geoCalc();`
        applyEffect();

        //alert( 'Sandbox: done.');

    } catch( e ) {
        writeLn('Une erreur est survenue.');
        alert( e, _config.scriptName );
    }        

    app.endUndoGroup();

})();
