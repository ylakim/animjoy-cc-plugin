﻿/*
 * Animjoy (launch from "Adobe Illustrator/Presets/fr_FR/Scripts" folder)
 */

//@target "illustrator"

/* global Animjoy */

/* global app */
/* global Folder */
/* global File */

(function() {
    
    /*
     * Locating current folder
     */
    var _currentScript      = (new File( $.fileName )),
        _folderFS           = Folder.decode( _currentScript.path );
    
    var _inAIScripts        = _folderFS.match(/Adobe Illustrator [\w\s]*\/Presets([\w\._\/]*)?Scripts\/?$/);
    
    if( !_inAIScripts ) {
        alert( 'Le fichier "' + _currentScript.name + '" doit être copié dans le dossier "Adobe Illustrator/Presets/fr_FR/Scripts/".' );
        return;
    }
    
    /*
     * Locating AE folder
     */
    var _pluginFolderName   = 'Animjoy-CC-Plugin',
        _appsFolder         = Folder('/Applications/'),
        _AE_Folders         = _appsFolder.getFiles( function(folder) {
            return ( folder instanceof Folder && Folder.decode( folder.name ).match(/Adobe After Effects [\w\s]*/) );
        });
        
    if( !_appsFolder || !_AE_Folders.length ) {
        alert( "L'emplacement de After Effects n'a pas été trouvé dans " + Folder.decode( _appsFolder.fsName ) );
        return;
    }
    
    _AE_Folders.sort( function( a, b ) {
        if( a.name < b.name )
            return -1;
        if( a.name > b.name )
            return 1;
        return 0;
    } ).reverse(); // anti-alphabetically
    
    
    /*
     * Locating AE plugin folder
     */
    var _AE_Folder      = _AE_Folders[0],
        _pluginFolder   = Folder( _AE_Folder.fsName + '/Scripts/' + _pluginFolderName );
    
    if( !_pluginFolder.exists ) {
        alert( 'Le dossier de ' + _pluginFolderName + ' est introuvable dans ' + Folder.decode( _AE_Folder.fsName ) );
        return;
    }
    
    /*
     * Launch script
     */
    var _scriptFile = File( _pluginFolder.fsName + '/Illustrator.jsx' );

    if( _scriptFile.exists ) {
        $.evalFile( _scriptFile );
        Animjoy.Illustrator.publishCurrentImage();
    } else {
        alert( "Script introuvable: " + File.decode( _scriptFile.fsName ) );
    }
    
})();
